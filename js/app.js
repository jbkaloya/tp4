

$(document).ready(function(){
    
    //Part 1
    
    $('div.type.pizza-type label').css("cursor", "pointer");
    
    $('div.type.pizza-type label').hover(function(){
        $(this).find('span').removeClass("no-display");
    }, function(){
        $(this).find('span').addClass("no-display");
    });
    
    //Part 2
    $('div.type.nb-parts input').change(function(){
        
        var parts = $(this).val();
 
        if(parts < 6){
            $("span.pizza-two").addClass("no-display");
            $("span.pizza-one").addClass("pizza-"+parts);
        } else {
            $("span.pizza-one").addClass("pizza-6");
            $("span.pizza-two").removeClass("no-display").addClass("pizza-"+(parts - 6));
        }

    });
    
    
    //Part 3
    $("button.next-step").click(function(){
        $(this).fadeOut();
        $('div.infos-client').removeClass("no-display");
        
    });
    
    
    //PArt4
    $("button.add").click(function(){
        $("div.addr").append('<input type="text"/>');
    });
    
    //Part5
     $("button.done").click(function(){
        $("div.headline").find('small').html("Merci "+$("input.name").val()+" ! Votre commande sera livrée dans 15 minutes");
        $('div.main').fadeOut();
    });
    
    //Part 6, question 8
    
    $('input').change(function(){
        
            var pizzaprice = $('div.pizza-type input:checked').data("price") * $('div.nb-parts input').val();
            $('div.extra input:checked').each(function(){
                
                pizzaprice = pizzaprice + $(this).data("price");
            });
        $("div.stick-right p").html(pizzaprice+" €");
    })
    
    
    
    
    
    
    
    
    
});
